# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('PyAppApi', '0008_auto_20170311_1701'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='mathstudent',
            name='fullname',
        ),
        migrations.AddField(
            model_name='mathstudent',
            name='firstname',
            field=models.CharField(default=datetime.datetime(2017, 3, 11, 11, 34, 23, 331324, tzinfo=utc), max_length=50),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='mathstudent',
            name='lastname',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('PyAppApi', '0006_auto_20170311_1526'),
    ]

    operations = [
        migrations.CreateModel(
            name='MathStudent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('profile_pic', models.ImageField(default=b'https://cdn2.iconfinder.com/data/icons/mixed-rounded-flat-icon/512/note-512.png', upload_to=b'')),
                ('mathematics', models.PositiveIntegerField(default=0)),
                ('physics', models.PositiveIntegerField(default=0)),
                ('chemistry', models.PositiveIntegerField(default=0)),
                ('hindi', models.PositiveIntegerField(default=0)),
                ('english', models.PositiveIntegerField(default=0)),
                ('environment', models.PositiveIntegerField(default=0)),
                ('fullname', models.ForeignKey(to='PyAppApi.AuthUser')),
            ],
        ),
        migrations.RemoveField(
            model_name='student',
            name='fullname',
        ),
        migrations.DeleteModel(
            name='Student',
        ),
    ]

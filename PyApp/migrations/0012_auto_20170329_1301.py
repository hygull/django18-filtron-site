# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('PyApp', '0011_docpost'),
    ]

    operations = [
        migrations.AddField(
            model_name='docpost',
            name='posted_by',
            field=models.CharField(default=b'Rishikesh Agrawani', max_length=50),
        ),
        migrations.AddField(
            model_name='docpost',
            name='profile_image',
            field=models.ImageField(default=b'Default.jpg', upload_to=b''),
        ),
        migrations.AlterField(
            model_name='docpost',
            name='image',
            field=models.ImageField(null=True, upload_to=b'', blank=True),
        ),
    ]

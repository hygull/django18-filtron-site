# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('PyApp', '0013_auto_20170329_1309'),
    ]

    operations = [
        migrations.AlterField(
            model_name='docpost',
            name='posted_by',
            field=models.CharField(default=b'Your close', max_length=50),
        ),
    ]

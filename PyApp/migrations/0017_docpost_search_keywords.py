# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('PyApp', '0016_auto_20170329_1510'),
    ]

    operations = [
        migrations.AddField(
            model_name='docpost',
            name='search_keywords',
            field=models.CharField(default=b'nice', max_length=50),
        ),
    ]

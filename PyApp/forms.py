from django import forms
from .models import AuthUser, Post, DocPost
 
class UserRegistrationForm(forms.ModelForm):
	class Meta:
		model = AuthUser
		fields=["firstname","lastname","email","age","gender"]
		# fields="__all__"

class ImagePostingForm(forms.ModelForm):
	class Meta:
		model = Post
		fields = ["title","description","image","posted_by"]

class DocPostForm(forms.ModelForm):
	class Meta:
		model = DocPost
		fields = ["image","title", "description","search_keywords","posted_by","profile_image"]

	def clean_title(self):
		title = self.cleaned_data["title"]
		if title:
			print "Title is Ok"
			return title
		else:
			print "Error (Title is blank)"
			raise forms.ValidationError("Title should not be blank")

	def clean_description(self):
		description = self.cleaned_data["description"]
		if description:
			print "Description is Ok"
			return description
		else:
			print "Error (Description is blank)"
			raise forms.ValidationError("Description should not be blank")


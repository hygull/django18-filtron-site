from django.shortcuts import render
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.shortcuts import redirect
from django.http import HttpResponse
from .models import AuthUser, Post,DocPost
from .forms import UserRegistrationForm, ImagePostingForm,DocPostForm
from django.core.files.images import get_image_dimensions
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q
# Create your views here.

logout_url = 'Logout'
login_url = 'Login' 

def home(request,id=id):
	all_cookies = request.COOKIES
	print "COOKIES => ",all_cookies
	if "pyapp_username" in all_cookies:
		print "Cookie named => pyapp_username found. So rendering home page."
		return render(request,"home.html",{"form":form,"logout_url":logout_url})
	else:
		print "Didn't find any cookie named => pyapp_username. So rendering the login page"
		# return render(request,"login.html",{"signup_url":"Sign Up"})
		return redirect("/pyapp/image-posts/")
		

def registration(request):
	if request.method == "POST":
		form = UserRegistrationForm(request.POST)
		if form.is_valid():
			form.save(commit=True)
			response = HttpResponse()
			print "Setting cookie..."
			# form.cleaned_data["email"]+"::"+form.cleaned_data["firstname"]+"::"+form.cleaned_data["lastname"]
			response.set_cookie("pyapp_username","OkHemMala")
			print "Successfully set the cookie."
		return redirect("/pyapp/auth-users/")
	else:
		form = UserRegistrationForm()
		return render(request,"registration.html",{"form":form})

def order_by_firstname(request):
	users=AuthUser.objects.all().order_by("firstname");
	return render(request,"auth_users.html",{"users":users,"order":"Order by firstname"})

def auth_users(request):
	users=AuthUser.objects.all()
	return render(request,"auth_users.html",{"users":users,"order":"Order by registration"})

def login(request):
	if request.method == "POST":
		print "Login(POST Request)"
		print request.POST
		response = render_to_response(request,"login.html",{})
		response.set_cookie("pyapp_username","PyCookie")
		return response
	else:
		print "Login(GET Request)"
		#response = render_to_reponse(request,"login.html",{},context_instance = RequestContext(request))
		return redirect("/pyapp/login/")

def image_posts(request):
	posts = Post.objects.all().order_by("-created_at")
	return render(request,"image_posts.html",{"posts":posts})

def post_a_new_image(request):
	if request.method=="POST":
		form = ImagePostingForm(request.POST or None, request.FILES or None)
		if form.is_valid():
			print "Creating a new post(images)"
			img = form.cleaned_data["image"]
			print img,type(img)
			dimension = get_image_dimensions(img)
			if dimension[0]<1200:
				print "Image width should be >= 1200"
				return render(request,"error.html",{"dimension_err":"Image width should be >= 1200<br><br>Current width : "+str(dimension[0])+"<br><br>Current resolution : "+str(dimension[0])+"x"+str(dimension[1])})
			else:
				if dimension[1]<800:
					print "Image height should be >= 800"
					return render(request,"error.html",{"dimension_err":"Image height should be >= 800<br><br>Current height : "+str(dimension[1])+"<br><br>Current resolution : "+str(dimension[0])+"x"+str(dimension[1])})
				else:
					form.save(commit=True)
			return render(request,"success.html",{})
		else:
			print "Form is not valid"
			print request.POST
			return render(request,"error.html",{})
	else:
		form = ImagePostingForm()
		return render(request,"post_a_new_image.html",{"form":form})

def success(request):
	return render(request,"success.html",{})

def error(request):
	return render(request,"error.html",{})

def logout(request):
	if "pyapp_username" in request.COOKIES:
		print "Deleting cookie..."
		request.delete_cookie("pyapp_username")
		print "Cookie successfully deleted."
		return render(request,"logout.html",{"logout_msg":"You successfully logged out"})
	else:
		print "Cookie is not set."
		return render(request,"error.html",{"cookie_err":"Cookie is not set.Register."})

def line_charts(request):
	return render(request,"line_charts.html",{})

def bar_charts(request):
	return render(request,"bar_charts.html",{})

def polar_area_charts(request):
	return render(request,"polar_area_charts.html",{})


def well_docs(request):
	q = request.GET.get("q")
	print q,"...."
	posts_list = posts_list = DocPost.objects.order_by("-created")
	print posts_list
	if q:
		posts_list = DocPost.objects.order_by("-created").filter(
		Q(title__icontains=q)|
		Q(description__icontains=q)|
		Q(posted_by__icontains=q)|
		Q(created__icontains=q)|
		Q(search_keywords__icontains=q)
		)
	# contact_list = Contacts.objects.all()
	print posts_list
	paginator = Paginator(posts_list, 5) # Show 5 contacts per page
	page = request.GET.get('page')
	try:
		posts = paginator.page(page)
	except PageNotAnInteger:
		# If page is not an integer, deliver first page.
		posts = paginator.page(1)
	except EmptyPage:
		# If page is out of range (e.g. 9999), deliver last page of results.
		posts = paginator.page(paginator.num_pages)

	return render(request, "well_docs.html",{"posts":posts})

def well_docs_create(request):
	form = DocPostForm( request.POST or None, request.FILES or None)
	message = ""
	if request.method == "POST":
		if form.is_valid():
			instance = form.save(commit=False)
			instance.save()
			print "Redirecting to /pyapp/well-docs/"
			return redirect("/pyapp/well-docs/")
		else:
			message = "<h5 style='color:red'>** <span style='color:green'>Title</span> and <span style='color:green'>Description</span> are required fields</h5>"+"<h5 style='color:red'>**<span style='color:green'>Search keywords</span> and <span style='color:green'>your name</span> are also required </h5>"
			form = DocPostForm()
	else:
		form = DocPostForm()

	context = {"form" : form,"message":message}
	return render(request, "well_docs_create.html",context)

def well_docs_single_view(request,id):
	post = get_object_or_404(DocPost,id=id)
	return render(request,"well_docs_single_view.html",{"post":post})
